#include <graphics.h>
#include <conio.h>

void main()
{
	int y[5],x[5];
	int vy[5]={0},vx[5]={0};
	initgraph(600, 400);
	//初始化球的位置
	int i,j;
	for(i=0;i<5;i++){
		x[i]=100+i*100;
		y[i]=150+i*50;
		vx[i]=40;
		vy[i]=40;
	}
	BeginBatchDraw();//开始批量绘制
	while(1)
	{
		for(i=0;i<5;i++)
		{
			// 绘制黄线、绿色填充的圆
			setcolor(YELLOW);
			setfillcolor(GREEN);
			fillcircle(x[i], y[i], 20);
		}
		FlushBatchDraw();
		// 延时
		Sleep(200);
		// 绘制黑线、黑色填充的圆
		for(i=0;i<5;i++)
		{
			setcolor(BLACK);
			setfillcolor(BLACK);
			fillcircle(x[i], y[i], 20);

			y[i]+=vy[i];
			x[i]+=vx[i];
			//碰到边界，改变方向
			if(y[i]<=40 || y[i]>=360)
				vy[i]=-vy[i];
			if(x[i]<=40 || x[i]>=560)
				vx[i]=-vx[i];
			//与球相撞，改变方向
			for(j=0;j<5;j++)
			{
				if(i==j)
					j++;
				else
				{
					if((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])<=1600)
					{
						vx[i]=-vx[i];
						vy[i]=-vy[i];
					}
				}
			}
		}
	}
	closegraph();
}